# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from the latest golang base image
FROM golang:latest

# Add Maintainer Info
LABEL maintainer="Gowrisankar <xyz@gmail.com>"

# Set the Current Working Directory inside the container

WORKDIR /app

# Copy everything from the current directory to the Working Directory inside the container
COPY main.go .

# Build the Go app
RUN go build main.go

# Expose port 8080 to the outside world
EXPOSE 8080

# Command to run the executable
CMD ["./main"]
